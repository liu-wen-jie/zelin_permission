# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

from __future__ import unicode_literals

import frappe


@frappe.whitelist()
def get_data_for_custom_field_wrapper(doctype, fieldname, field=None):
    from frappe.desk.query_report import get_data_for_custom_field

    dummy_doc = frappe.get_doc({'doctype': doctype})
    try:
        if dummy_doc.has_permlevel_access_to(field or fieldname):
            return get_data_for_custom_field(doctype = doctype, fieldname=fieldname, field=field)
        else:
            frappe.msgprint(_('No field permission'))
    except Exception as e:
        frappe.msgprint(f"Add Column for {doctype} {fieldname} with error {str(e)}")        
